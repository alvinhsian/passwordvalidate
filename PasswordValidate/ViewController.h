//
//  ViewController.h
//  PasswordValidate
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/4/15.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIAlertViewDelegate>

- (IBAction)toShow:(id)sender;

@end

