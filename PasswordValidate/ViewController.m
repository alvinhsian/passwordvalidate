//
//  ViewController.m
//  PasswordValidate
//
//  Created by 網際優勢(股)公司Linda Lin on 2015/4/15.
//  Copyright (c) 2015年 uxb2b. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

NSInteger loginErrorCount = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)toShow:(id)sender {
    [self showAlertView:@"請登入"];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    //如果按下"登入"
    if (buttonIndex == 1) {
        //判斷使用者輸入的密碼是否正確
        //[alertView textFieldAtIndex:0].text => 帳號
        //[alertView textFieldAtIndex:1].text => 密碼
        if ([[alertView textFieldAtIndex:1].text isEqualToString:@"12345"]) {
            [self performSegueWithIdentifier:@"loginSuccess" sender:nil];
        } else {
            //登入失敗
            //累計登入錯誤次數
            loginErrorCount++;
            
            if (loginErrorCount <= 3 ) {
                
                NSString *errMsg = [NSString stringWithFormat: @"登入錯誤累計%ld次，請重新登入", (long)loginErrorCount];
                
                [self showAlertView:errMsg];
                
            }else if(loginErrorCount > 3){
                
                //登入錯誤超過3次
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"登入錯誤超過3次" message:nil delegate:self cancelButtonTitle:@"確定" otherButtonTitles:nil];
                
                [alertView show];
            }
        }
    }

}



//顯示彈出登入對話方塊(傳入一個字串當作畫面狀態)
-(void)showAlertView:(NSString *)loginStatus{
    
    UIAlertView *loginView = [[UIAlertView alloc] initWithTitle:loginStatus message:@"請輸入帳號密碼" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"登入", nil];
    
    //設定彈出視窗為可輸入帳號密碼
    [loginView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    [loginView show];
}
@end
